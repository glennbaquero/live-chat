
## Database
- Create database
- Duplicate the .env.example file and rename to .env
- Change the following line of codes in .env

```php
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password
```

## Installation
 Run the following command
- npm install
- composer update or composer install
- php artisan migrate && php artisan db:seed

## URL
- Admin
	- Login
		- /admin/login
 	- Create User
   		- /admin/users

- User
	- Login
		- /login
 	- Chat
   		- chat


## Credentials
- Admin
	- email : admin@chat.com
	- password: password
- User
	- username : user1
	- password: password
	