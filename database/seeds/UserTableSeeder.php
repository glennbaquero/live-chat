<?php

use Illuminate\Database\Seeder;

use App\Models\Users\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $users = [
        	[
        	    'name' => 'John Doe',
        	    'username' => 'user1',
        	    'password' => 'password',
        	],
        	[
        	    'name' => 'Jane Doe',
        	    'username' => 'user2',
        	    'password' => 'password',
        	],
        ];

    	foreach($users as $user) {
    	    $user['password'] = Hash::make($user['password']);
    	    $user['email_verified_at'] = now();
    	    
    	    User::create($user);
    	}
    }
}
