<?php

use Illuminate\Database\Seeder;

use App\Models\Users\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();

        $admins = [
        	[
        	    'name' => 'Super Admin',
        	    'email' => 'admin@chat.com',
        	    'password' => 'password',
        	],
        ];

    	foreach($admins as $admin) {
    	    $admin['password'] = Hash::make($admin['password']);
    	    $admin['email_verified_at'] = now();
    	    
    	    Admin::create($admin);
    	}
    }
}
