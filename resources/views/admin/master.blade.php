<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        @include('admin.partials.styles')

        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div id="app" class="wrapper">
            {{-- @include('admin.partials.header') --}}
            {{-- @include('admin.partials.sidebar') --}}

            
            @yield('content')

            {{-- @include('admin.partials.footer') --}}

        </div>

        @include('admin.partials.script-tags')
    </body>
</html>
