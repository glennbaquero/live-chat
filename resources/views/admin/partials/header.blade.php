<nav class="main-header navbar navbar-expand border-bottom navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link text-dark" data-widget="pushmenu" href="javascript:void(0)"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item d-none d-sm-inline-block">
            <a class="nav-link" href="">
                <i class="fa fa-bell mr-2 text-dark"></i>
            
            </a>
        </li>

        <li class="nav-item d-none d-sm-inline-block">
            <a class="nav-link" href="">Logout</a>
        </li>

    </ul>
</nav>
