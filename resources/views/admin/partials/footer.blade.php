<footer class="main-footer overflow-hidden">
    
    <div class="float-right d-none d-sm-block">
		<b>Version</b> 0.1
    </div>
	
	<strong>Copyright &copy; {{ now()->year }} <a class="text-dark" href="javascript:void(0)">{{ config('app.name') }}</a>.</strong> All rights reserved.
</footer>