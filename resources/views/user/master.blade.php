<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        @include('user.partials.styles')

        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <script type="text/javascript" src="{{ asset('peerjs.min.js') }}"></script> --}}
    </head>
    <body>
        <div id="app" class="wrapper">
            {{-- @include('user.partials.header') --}}
            {{-- @include('user.partials.sidebar') --}}

            
            @yield('content')

            {{-- @include('user.partials.footer') --}}

        </div>

        @include('user.partials.script-tags')
    </body>
</html>
