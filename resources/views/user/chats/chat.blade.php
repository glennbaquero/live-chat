@extends('user.master')
@section('content')
<div class="messaging">
    <chat
    	submit-url="{{ route('user.chat.store-peer') }}"
    	fetch-url="{{ route('user.chat.peer-fetch') }}"
    	truncate-peer-url="{{ route('user.chat.peer-truncate') }}"
    	chat-submit-url="{{ route('user.chat.message-store') }}"
    	logged-user="{{ auth()->guard('web')->user()->name }}"
    ></chat>
</div>
@endsection