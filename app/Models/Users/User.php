<?php

namespace App\Models\Users;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Models\Conversations\Conversation;

use Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The attributes that should be append.
     *
     * @var array
     */
    protected $appends = [
        'showUrl', 'removeUrl'
    ];

    /**
     * Relationships
     */

    public function conversations() {
        return $this->hasMany(Conversation::class, 'sender_id', 'id');
    }


    /**
     * @Setters
     */
    public static function store($request, $item = null, $columns = ['name', 'username'])
    {
        
        $vars = $request->only($columns);

        if($request->filled('password')) {
            $vars['password'] = Hash::make($request->password);
        }
        $vars['email_verified_at'] = now();


        if (!$item) {
            $item = static::create($vars);
        } else {
            $item->update($vars);
        }

        return $item;
    }

    /**
     * Renderers
     */
    
    public function renderShowUrl() {
        return route('users.show', $this->id);
    }

    public function renderRemoveUrl() {
        return route('users.remove', $this->id);
    }

    /**
     * Appends
     */
    
    public function getShowUrlAttribute() {
        return $this->renderShowUrl();
    }

    public function getRemoveUrlAttribute() {
        return $this->renderRemoveUrl();
    }
}
