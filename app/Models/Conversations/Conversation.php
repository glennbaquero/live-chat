<?php

namespace App\Models\Conversations;

use Illuminate\Database\Eloquent\Model;

use App\Models\Users\User;
use App\Models\Peers\Peer;

class Conversation extends Model
{
	protected $guarded = [];
	
    /**
     * Relatioships
     */
    
    public function sender() {
    	return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function peer() {
    	return $this->belongsTo(Peer::class);
    }
}
