<?php

namespace App\Models\Peers;

use Illuminate\Database\Eloquent\Model;

use App\Models\Conversations\Conversation;

class Peer extends Model
{
    protected $guarded = [];

    /**
     * Relationships
     */
    
    public function conversations() {
    	return $this->hasMany(Conversation::class);
    }
}
