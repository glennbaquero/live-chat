<?php

namespace App\Helpers;

use Auth;

class AuthHelper
{
	public static function getGuard($request) {
		$class = get_class($request->user());

		switch ($class) {
			case 'App\Models\Users\User':
				return 'web';
		}

		return false;
	}

	public function renderName($format = 'f l') {
		$result = null;

		if (auth()->check()) {
			$result = auth()->user()->name;
		}

		return $result;
	}

	public function authenticated($guard = null) {
		return auth($guard)->check();
	}
}