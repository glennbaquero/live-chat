<?php

namespace App\Http\Controllers\User\Chats;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Peers\Peer;
use App\Models\Conversations\Conversation;
use DB;

class ChatController extends Controller
{
	/**
	 * Index view
	 */
	
	public function index()
	{
		return view('user.chats.chat');
	}

   	/**
   	 * insert new chat message
   	 * @param $request
   	 * @return Object list of peer
   	 */
   	
	public function storeMessage(Request $request)
	{
		$peer = Peer::first()->id;

		$request['peer_id'] = $peer;

		DB::beginTransaction();
			auth()->guard('web')->user()->conversations()->create($request->only(['message', 'peer_id']));
		DB::commit();

		return response()->json([
			true
		]);
	}

   	/**
   	 * insert new peer connection if no available peer id\
   	 * @param $request
   	 * @return Object list of peer
   	 */
   	
   	public function storePeer(Request $request)
   	{
   		DB::beginTransaction();
   			$item = Peer::create($request->all());
   		DB::commit();

   		return response()->json([
   			'peer' => $item
   		]);
   	}

      public function truncatePeer(Request $request)
      {

         DB::beginTransaction();
            $item = Peer::truncate();
         DB::commit();

         return response()->json([
            true
         ]);
      }


   	/**
   	 * fetch all peer connection available
   	 * @param $request
   	 * @return Boolean check if the peer is available
   	 * @return Object peer details
   	 */
   	
   	public function fetchPeer(Request $request)
   	{
   		DB::beginTransaction();
   			$item = Peer::first();
   		DB::commit();

   		return response()->json([
   			'channelIsAvailable' => $item ? true : false,
   			'peer' => $item
   		]);
   	}
}
