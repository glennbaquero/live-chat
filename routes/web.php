<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//////////////////
// Admin Routes //
//////////////////

Route::namespace('Admin\Auth')->prefix('admin')->middleware('guest:admin')->group(function() {

    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');

    Route::get('reset-password/{token}/{email}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('reset-password/change', 'ResetPasswordController@reset')->name('password.change');

    Route::get('forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('forgot-password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

});

Route::namespace('Admin')->prefix('admin')->middleware('auth:admin')->group(function() {

    Route::namespace('Users')->group(function() {

        Route::get('users', 'UserController@index')->name('users.index');
        Route::get('users/create', 'UserController@create')->name('users.create');
        Route::post('users/store', 'UserController@store')->name('users.store');
        Route::get('users/show/{id}', 'UserController@show')->name('users.show');
        Route::post('users/update/{id}', 'UserController@update')->name('users.update');
        Route::get('users/remove/{id}', 'UserController@destroy')->name('users.remove');
        Route::post('users/fetch', 'UserController@fetch')->name('users.fetch');

    });

});



/////////////////
// User Routes //
/////////////////

Route::namespace('User\Auth')->name('user.')->middleware('guest:web')->group(function() {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');
});

Route::namespace('User')->name('user.')->middleware('auth:web')->group(function() {
    Route::namespace('Chats')->group(function() {
        Route::get('/chat', 'ChatController@index')->name('chat');
        Route::post('/message/store', 'ChatController@storeMessage')->name('chat.message-store');
        Route::post('/store-peer', 'ChatController@storePeer')->name('chat.store-peer');
        Route::post('/truncate-peer', 'ChatController@truncatePeer')->name('chat.peer-truncate');
        Route::post('/fetch-peer', 'ChatController@fetchPeer')->name('chat.peer-fetch');
    });
});

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
